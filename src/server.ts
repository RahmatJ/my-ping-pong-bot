import dotenv from 'dotenv';
import Discord from 'discord.js';

dotenv.config();
const main = () => {
  const intents: any[] = [
    Discord.GatewayIntentBits.Guilds,
    Discord.GatewayIntentBits.GuildMessages,
    Discord.GatewayIntentBits.MessageContent
  ];
  const client = new Discord.Client({ intents });

  client.on('ready', () => {
    if (!client.user) {
      console.log('user not found');
      throw new Error('user not found');
    }
    console.log(`Logged in as ${client.user.tag}! we are online`);
  });

  client.on('messageCreate', (msg) => {
    console.log(msg);
    if (msg.content.substring(0, 1) === '!') {
      const args = msg.content.substring(1).split(' ');
      const cmd = args[0];

      console.log({ args });

      switch (cmd) {
        case 'ping':
          msg.channel.send(`Hello from AI bot ${msg.author.username}`);
          break;
        case 'help':
          msg.channel.send(`This will show help page`);
          break;
        default:
          msg.channel.send(`Unknown command, please try !help`);
      }
    }
  });

  client.login(process.env.CLIENT_TOKEN);
};

main();
